Context: https://github.com/Rust-for-Linux/linux/pull/875

``proc_handler()`` receives a user pointer since::

    commit 32927393dc1ccd60fb2bdc05b9e8e88753761469
    Date:   Fri Apr 24 08:43:38 2020 +0200
    
        sysctl: pass kernel pointers to ->proc_handler

Example of the EFAULT:

    # insmod phandler.ko
    # ls -l /proc/sys/bug/
    total 0
    -rw-rw-rw- 1 root root 0 2022-08-29 08:04 ouch
    # cat /proc/sys/bug/ouch
    cat: /proc/sys/bug/ouch: Bad address
    # echo 1 >/proc/sys/bug/ouch
    -bash: echo: write error: Bad address
