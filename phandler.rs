// SPDX-License-Identifier: GPL-2.0
/*! No, ``proc_handler()`` does not receive a user pointer. */

use kernel::prelude::*;
use kernel::sysctl::Sysctl;
use kernel::Mode;
use kernel::c_str;

use core::sync::atomic::{AtomicBool, Ordering};

module! {
    type: Phandler,
    name: "phandler",
    author: "Philipp Gesang",
    description: "sysctl issue demo",
    license: "GPL",
}

struct Phandler {
    sysctl: Sysctl<AtomicBool>,
}


impl kernel::Module for Phandler {
    fn init(_name: &'static CStr, _module: &'static ThisModule) -> Result<Self> {
        pr_info!("init\n");

        let sysctl = Sysctl::register(c_str!("bug"),
                                      c_str!("ouch"),
                                      AtomicBool::new(false),
                                      Mode::from_int(0o666))?;
        pr_info!("sysctl created with value {}\n",
                 sysctl.get().load(Ordering::Relaxed));

        Ok(Phandler { sysctl })
    }
}

impl Drop for Phandler {
    fn drop(&mut self) {
        pr_info!("exit. value: {}\n",
                 self.sysctl.get().load(Ordering::Relaxed));
    }
}
